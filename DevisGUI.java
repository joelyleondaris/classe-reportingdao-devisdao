

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.List;


/**
 * Classe DevisFenetre
 * Définit et ouvre une fenetre qui :
 * 
 *    - Permet l'insertion d'un nouveau Devis dans la table Devis via
 * la saisie des identifiant des Devis, le prix, le prix de la main d'oeuvre, 
 * la maintenance associé
 *    - Permet l'affichage de tous les Devis dans la console
 *    
 * @author Chokote & Leondaris
 * @version 1.3
 * */


public class DevisGUI extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class DevisFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * DevisFenetre
	 */
	private JPanel containerPanel;

	/**
	 * zone de texte pour le champ identifiant CDevis
	 */
	private JTextField textFieldIdentifiant;

	/**
	 * zone de texte pour le champ prix
	 */
	private JTextField textFieldPrix;

	/**
	 * zone de texte pour le prix de la main d'oeuvre
	 * 
	 */
	private JTextField textFieldPrixMdo;
	/**
	 * zone de texte pour la maintenance associé
	 * 
	 */
	private JTextField textFieldMaintenance;

	
	private JTextField textFieldRecherche;

	
	
	/**
	 * label identifiant du Devis
	 */
	private JLabel labelIdentifiant;

	/**
	 * label prix
	 * 
	 */
	
	private JLabel labelPrix;

	/**
	 * label prix de la main d'oeuvre
	 */
	private JLabel labelPrixMdo;

	/**
	 * label maintenance
	 */
	private JLabel labelMaintenance;
	

	
	private JLabel labelRecherche;
	
	
	

	/**
	 * bouton d'ajout du Devis
	 */
	private JButton boutonAjouter;
	
	private JButton boutonRecherche;

	/**
	 * bouton qui permet d'afficher tous les Devis
	 */
	private JButton boutonAffichageTousLesDevis;
	
	private JButton boutonQuitter;

	/**
	 * Zone de texte pour afficher les Devis
	 */
	private JTextArea zoneTextListDevis;
	
	private JTextArea zoneTextDevis;

	/**
	 * Zone de défilement pour la zone de texte
	 */
	private JScrollPane zoneDefilement;

	
	
	/**
	 * instance de DevisDAO permettant les accès à la base de données
	 */
	private DevisDAO monDevisDAO;

	/**
	 * Constructeur Définit la fenêtre et ses composants - affiche la fenêtre
	 */
	public DevisGUI() {
		// on instancie la classe Devis DAO
		this.monDevisDAO = new DevisDAO ();
		

		// on fixe le titre de la fenêtre
		this.setTitle("Devis");
		// initialisation de la taille de la fenêtre
		this.setSize(400, 400);

		// création du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de gérer la position des éléments
		// il autorisera un retaillage de la fenêtre en conservant la
		// présentation
		// BoxLayout permet par exemple de positionner les élements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(Color.PINK);

		// instantiation des composants graphiques
		textFieldIdentifiant = new JTextField();
		textFieldPrix = new JTextField();
		textFieldPrixMdo = new JTextField();
		textFieldMaintenance = new JTextField();
		
		textFieldRecherche = new JTextField();
		
		boutonAjouter = new JButton("Ajouter");
		boutonRecherche = new JButton ("Rechercher");
		boutonAffichageTousLesDevis = new JButton(
				"Afficher tous les Devis");
		boutonQuitter = new JButton ("Quitter");
		labelIdentifiant = new JLabel("Identifiant Devis :");
		labelPrix = new JLabel("Prix :");
		labelPrixMdo = new JLabel("Prix de la main d'oeuvre :");
		labelMaintenance = new JLabel("Maintenance associe :");
		
		labelRecherche = new JLabel ("Rechercher :");

		zoneTextDevis = new JTextArea(10, 20);
		zoneTextDevis.setEditable(false);
		
		zoneTextListDevis = new JTextArea(10, 20);
		zoneDefilement = new JScrollPane(zoneTextListDevis);
		zoneTextListDevis.setEditable(false);

		// ajout des composants sur le container
		containerPanel.add(labelIdentifiant);
		// introduire une espace constant entre le label et le champ texte
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldIdentifiant);
		// introduire une espace constant entre le champ texte et le composant
		// suivant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		
		containerPanel.add(labelPrix);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldPrix);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelPrixMdo);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldPrixMdo);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));

		containerPanel.add(labelMaintenance);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(textFieldMaintenance);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));


		containerPanel.add(boutonAjouter);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(labelRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(boutonRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		//containerPanel.add(textFieldRecherche);
		//containerPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		//containerPanel.add(zoneTextClient);
		

		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));

		containerPanel.add(boutonAffichageTousLesDevis);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		containerPanel.add(zoneDefilement);
		
		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		containerPanel.add(boutonQuitter);

		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 10, 10, 10));

		// ajout des écouteurs sur les boutons pour gérer les évènements
		boutonAjouter.addActionListener(this);
		boutonAffichageTousLesDevis.addActionListener(this);
		boutonQuitter.addActionListener(this);

		// permet de quitter l'application si on ferme la fenêtre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);

		// affichage de la fenêtre
		this.setVisible(true);
	}

	/**
	 * Gère les actions réalisées sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		int retour; // code de retour de la classe ArticleDAO
		MaintenanceDAO maintenanceDAO = new MaintenanceDAO();
		try {
			if (ae.getSource() == boutonAjouter) {
				// on crée l'objet message
				Devis a = new Devis(
						Double.parseDouble(this.textFieldIdentifiant.getText()),
						Double.parseDouble(this.textFieldPrix.getText()),
						Double.parseDouble(this.textFieldPrixMdo.getText()), 
						maintenanceDAO.getMaintenance(Double.parseDouble(this.textFieldMaintenance.getText())));
						
						
				// on demande à la classe de communication d'envoyer le client
				// dans la table Client
				retour = monDevisDAO.ajouter(a);
				// affichage du nombre de lignes ajoutées
				// dans la bdd pour vérification
				System.out.println("" + retour + " ligne ajoutee ");
				if (retour == 1)
					JOptionPane.showMessageDialog(this, "Devis ajoutee !");
				else
					JOptionPane.showMessageDialog(this, "Erreur ajout Devis",
							"Erreur", JOptionPane.ERROR_MESSAGE);
			} else if (ae.getSource() == boutonAffichageTousLesDevis) {
				// on demande à la classe DevisDAO d'ajouter le message
				// dans la base de données
				List<Devis> liste = monDevisDAO.getListeDevis();
				// on efface l'ancien contenu de la zone de texte
				zoneTextListDevis.setText("");
				// on affiche dans la console du Devis les Devis reçus
				for (Devis a : liste) {
					zoneTextListDevis.append(a.toString());
					zoneTextListDevis.append("\n");
					// Pour afficher dans la console :
					 System.out.println(a.toString());
				}
			} else if (ae.getSource() == boutonRecherche){
				
				zoneTextDevis.append(monDevisDAO.getDevis(Long.parseLong(this.textFieldRecherche.getText())).toString());
			}
			else if (ae.getSource() == boutonQuitter){
				this.dispose();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this,
					"Veuillez contrôler vos saisies", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			System.err.println("Veuillez contrôler vos saisies");
		}

	}

	public static void main(String[] args) {
		new DevisGUI ();
	}

}