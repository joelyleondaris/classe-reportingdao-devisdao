import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'accès aux données contenues dans la table client
 * 
 * @author Chokote Léondaris
 * @version 1.2
 * */
public class ReportingDAO {

	/**
	 * Paramètres de connexion à la base de données oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "SYSTEM";  //exemple BDD1
	final static String PASS = "jorim1996";   //exemple BDD1

	
	/**
	 * Constructeur de la classe
	 * 
	 */
	public ReportingDAO() {
		// chargement du pilote de bases de données
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}

	


	

	/**
	 * Permet de récupérer tous les clients stockés dans la table client_clt
	 * 
	 * @return une ArrayList de Client
	 */
	public List<Operateur> reportingOperateurQuotidien(java.sql.Date date) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Operateur> retour = new ArrayList<Operateur>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM maintenance_mtn INNER JOIN operateur_opr ON maintenance_mtn.id_mtn_opr = operateur_opr.id_opr WHERE date_mtn = ? ");
			ps.setDate(1, date);

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Operateur(rs.getDouble("id_opr"), rs.getString("nom_opr"), rs.getString("pre_opr"), rs.getLong("tel_opr"), rs.getDate("opr_date_mes")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	public List<Client> reportingClientQuotidien(java.sql.Date date) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Client> retour = new ArrayList<Client>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM maintenance_mtn INNER JOIN client_clt ON maintenance_mtn.id_mtn_clt = client_clt.id_clt WHERE date_mtn = ? ");
			ps.setDate(1, date);

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Client(rs.getDouble("id_clt"), rs
						.getString("ent_clt"), rs
						.getLong("siret_clt"), rs
						.getString("adr_clt"), rs.getString("ape_clt")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}
	
	public List<Client> reportingClientHebdomadaire(java.sql.Date date) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Client> retour = new ArrayList<Client>();

		// connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM maintenance_mtn INNER JOIN client_clt ON maintenance_mtn.id_mtn_clt = client_clt.id_clt WHERE date_mtn = ? ");
			ps.setDate(1, date);

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Client(rs.getDouble("id_clt"), rs
						.getString("ent_clt"), rs
						.getLong("siret_clt"), rs
						.getString("adr_clt"), rs.getString("ape_clt")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	// main permettant de tester la classe
	public static void main(String[] args) throws SQLException {

		ReportingDAO clientDAO = new ReportingDAO();
		

		// test de la méthode getListeClient
		List<Operateur> liste = clientDAO.reportingOperateurQuotidien(Date.valueOf("2017-05-17"));
		// affichage des articles
		for (Operateur clt : liste) {
			System.out.println(clt.toString());
		}

	}
}
